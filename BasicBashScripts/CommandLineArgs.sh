#!/bin/bash

# Using command line arguments.
echo "To use: CommandLineArgs.sh Thing1 Thing2 Thing3"

POS1="$1"
POS2="$2"
POS3="$3"

echo "$1 is the first position parameter \$1"
echo "$2 is the same as $POS2 the second position parameter \$2"
echo "$1 is the first position parameter \$1"
