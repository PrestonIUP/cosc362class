#!/bin/bash

# A script with a variable
# Convention in a bash script is to use Capitals for variables
# This is not required.

TEXT="My cool text."
NOTCOOL="My not cool text."

echo "Some stuff" $TEXT
echo "Some other stuff ${NOTCOOL}"

