// This is just a place where I put the signatures for the
// Funtions I'm building in HelperFunctions.c

//int Function(Called);
int Distance(int x1, int y1, int x2, int y2);

int posHalf(int num, int num2);

int negHalf(int num);

int CheckRadius(int totalRows, int totalCols, int radius, int pixRow, int pixCol);

int Stem(int totalRows, int totalCols, int radius, int pixRow, int pixCol);

int Mouth(int totalRows, int totalCols, int radius, int pixRow, int pixCol);

int Eye(int totalRows, int totalCols, int pixRow, int pixCol);

