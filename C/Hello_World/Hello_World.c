#include <stdio.h>
#include <stdlib.h>

/*
* This is a Hello World! in C programming
* Author: Preston Dulion	Date: 10/14/2021
*/


int main(int argc, char *argv[] ){

	printf("\nHello World!\n");

	return 0;
}
