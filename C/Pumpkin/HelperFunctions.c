#include <math.h> //Lets me use pow(stuff,power)

/* This is the helper function file for my Pumpkin */

int CheckRadius(int totalRows, int totalCols, int radius, int pixRow, int pixCol){

	int InOrOut   = 0;		 //Returns 0 or 1 for out and in side the cricle respectively.
	int centerRow = totalRows / 2;   //Place holder for the center row.
	int centerCol = totalCols / 2;	 //Place holder for the center column.
	int dist      = 0;

//	printf("Inside CheckRadius, HelperFunctions.c\n");

	//dist = pow((pow(centerRow-pixRow, 2) + pow(centerCol-pixCol, 2)), 0.5);
	dist = Distance(pixRow, pixCol, centerCol, centerRow);

	if( dist < radius ) {
		InOrOut = 1;
	}

	return InOrOut;
}

int Stem(int totalRows, int totalCols, int radius, int pixRow, int pixCol){

	int inStem = -1;
	int height = radius/4, length = radius/6;
	int centerRow = totalRows / 2;   //Place holder for the center row.
	int centerCol = totalCols / 2;	 //Place holder for the center column.
	//Gets half way points from the center to the end of the image in all cardinal directions
	int quarterRow[2];
	int quarterCol[2];

	int eighthRow[4];
	int eighthCol[4];

	quarterRow[0] = posHalf(centerRow, centerRow);
	quarterRow[1] = negHalf(centerRow);

	quarterCol[0] = posHalf(centerCol, centerCol);
	quarterCol[1] = negHalf(centerCol);

	eighthRow[0]  = posHalf(quarterRow[0], quarterRow[1]);
	eighthRow[1]  = negHalf(quarterRow[0]);
	eighthRow[2]  = posHalf(quarterRow[1], quarterRow[0]);
	eighthRow[3]  = negHalf(quarterRow[1]);

	eighthCol[0]  = posHalf(quarterCol[0], quarterCol[1]);
	eighthCol[1]  = negHalf(quarterCol[0]);
	eighthCol[2]  = posHalf(quarterCol[1], quarterCol[0]);
	eighthCol[3]  = negHalf(quarterCol[1]);

	/*if(pixRow == eighthRow[3] || pixCol == centerCol){
		inStem = 3;
	}*/

	if((pixRow > (eighthRow[3] - height) && pixRow < (eighthRow[3] + height)) && (pixCol > (centerCol - length) && pixCol < (centerCol + length))){
		inStem = 1;
	}

	return inStem;
}
int Mouth(int totalRows, int totalCols, int radius, int pixRow, int pixCol){

	int inMouth = -1;
	int dist = 0;
	int centerRow = totalRows / 2;   //Place holder for the center row.
	int centerCol = totalCols / 2;	 //Place holder for the center column.
	//Gets half way points from the center to the end of the image in all cardinal directions
	int quarterRow[2];
	int quarterCol[2];

	int eighthRow[4];
	int eighthCol[4];

	quarterRow[0] = posHalf(centerRow, centerRow);
	quarterRow[1] = negHalf(centerRow);

	quarterCol[0] = posHalf(centerCol, centerCol);
	quarterCol[1] = negHalf(centerCol);

	eighthRow[0]  = posHalf(quarterRow[0], quarterRow[1]);
	eighthRow[1]  = negHalf(quarterRow[0]);
	eighthRow[2]  = posHalf(quarterRow[1], quarterRow[0]);
	eighthRow[3]  = negHalf(quarterRow[1]);

	eighthCol[0]  = posHalf(quarterCol[0], quarterCol[1]);
	eighthCol[1]  = negHalf(quarterCol[0]);
	eighthCol[2]  = posHalf(quarterCol[1], quarterCol[0]);
	eighthCol[3]  = negHalf(quarterCol[1]);

	dist = Distance(pixRow, pixCol, centerCol, centerRow);

	if( (dist < (radius/1.5)) && pixRow > eighthRow[2] ) {
		inMouth = 1;
	}

	return inMouth;

}


//Variables used for Eye function, primarily used as a tracker.
int iterationHeight 	 = 0;
int iterationLength	 	 = 0;
int iterationLengthRight = 0;
// int iterationNose 		 = 0;
// int iterationHeightNose  = 0;

int Eye(int totalRows, int totalCols, int pixRow, int pixCol){

	int inEye     = -1;		 //Returns 0 or 1 for out and in side the eye respectively.
	int eyeFlag = 0; 		 //Set to 0 or >0, >0 meaning inside the eye triangle at various points, 0 meaning outside it.
	int centerRow = totalRows / 2;   //Place holder for the center row.
	int centerCol = totalCols / 2;	 //Place holder for the center column.
	//Gets half way points from the center to the end of the image in all cardinal directions
	int quarterRow[2];
	int quarterCol[2];

	int eighthRow[4];
	int eighthCol[4];

	quarterRow[0] = posHalf(centerRow, centerRow);
	quarterRow[1] = negHalf(centerRow);

	quarterCol[0] = posHalf(centerCol, centerCol);
	quarterCol[1] = negHalf(centerCol);

	eighthRow[0]  = posHalf(quarterRow[0], quarterRow[1]);
	eighthRow[1]  = negHalf(quarterRow[0]);
	eighthRow[2]  = posHalf(quarterRow[1], quarterRow[0]);
	eighthRow[3]  = negHalf(quarterRow[1]);

	eighthCol[0]  = posHalf(quarterCol[0], quarterCol[1]);
	eighthCol[1]  = negHalf(quarterCol[0]);
	eighthCol[2]  = posHalf(quarterCol[1], quarterCol[0]);
	eighthCol[3]  = negHalf(quarterCol[1]);

	//Triangle Eye
	int leftEyePosCol 	= quarterCol[0] + iterationLength;
	int rightEyePosCol	= quarterCol[3] + iterationLengthRight;
	int leftEnd 		= quarterCol[0] + iterationHeight;	
	int rightEnd 		= quarterCol[3] + iterationHeight;
	int eyePosRow 		= quarterRow[1] + iterationHeight;
	
	// //Triangle nose. Or a third eye.
	// int nosePosCol		= centerCol + iterationNose;
	// int nosePosRow 		= eighthRow[1] + iterationHeight;
	// int noseEnd			= centerCol + iterationHeight;

	if((pixCol == leftEyePosCol && pixRow == eyePosRow)){
		eyeFlag = 1;
		inEye = 0;
	}
	if((pixCol == rightEyePosCol && pixRow == eyePosRow)){
		eyeFlag = 2;
		inEye = 0;
	}

	if(pixRow == eyePosRow && pixCol == leftEnd){
		eyeFlag = 3;
		inEye = 0;
	}
	if(pixRow == eyePosRow && pixCol == rightEnd){
		eyeFlag = 4;
		inEye = 0;
	}
	if(pixRow == eighthRow[1] && pixCol == eighthCol[1]){
		inEye = 0;
		return inEye;
	}
	if(pixRow == eighthRow[1] && pixCol == eighthCol[0]){
		inEye = 0;
		return inEye;
	}

	if( eyeFlag == 0){
		iterationLength 	 = -(iterationHeight);
		iterationLengthRight = -(iterationHeight);
	}
	if(eyeFlag > 0){

			if( eyeFlag == 1 ){//|| eyeFlag != 3){
			iterationLength++;
		}

		if(eyeFlag == 2 ){//|| eyeFlag != 4){
			iterationLengthRight++;
		}

		if(iterationLength >= iterationHeight && (pixCol == leftEnd )){
			iterationHeight++;
		} 
	}
	return inEye;
}

//Takes 2 inputs of coordinates (x, y) separated into 4 ints
//and calculates the distance between the two coordinates.
int Distance(int x1, int y1, int x2, int y2){
	return pow((pow(x2-x1, 2) + pow(y2-y1, 2)), 0.5);
}

int posHalf(int num, int num2){
	return num + (num2 / 2);
}
int negHalf(int num){
	return num / 2;
}
