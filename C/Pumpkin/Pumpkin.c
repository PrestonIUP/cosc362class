#include <stdio.h>
#include <stdlib.h>
#include "MyFunctions.h"

//Test
/*
 * This is a file to make a pumpkin in a pixel map (.ppm)  code example in C programming
 * Author: Preston Dulion	Date: 10/28/2021
 */


int main(int argc,		//The number of command line arguments.
	 char *argv[] ){	//Vector of command line arguments

	//Variable declarations

	int numRows;		//Number of rows for the image file
	int numCols;		//Number of cols for the image file
	int imageSize;		//Total size of the .ppm file image
	int row, col;		//The marker to know where we are in the image.
	int Radius;		//This will be the size of the circle to put image center.
	int inOrOut;		//Flag for in or out of the circle/
	int inEye;			//Flag for in or out of the eyes
	int inMouth;		//Flag for in or out of the mouth
	int inStem;			//Flag for in or out of the stem

	unsigned char *outImage;	//Pixel pointer
	unsigned char *ptr;		//pointer
	FILE *outputFP;			//My output file pointer


	printf("========== Making a pumpkin .ppm file! ========== \n");

	//Read in the command  line arguments:

	if(argc != 5){
		printf("\n===== Usage: Pumpkin OutFileName.ppm numRows(100) numCols(100) Radius(50) =====\n");
		return 1; //exit(1); //can also be used
	}

	if((numRows = atoi(argv[2])) <= 0){
		printf("===== Enter a value greater than zero for rows. =====\n");
	}
	if((numCols = atoi(argv[3])) <= 0){
		printf("===== Enter a value greater than zero for cols. =====\n");
	}
	if((Radius = atoi(argv[4])) <= 0){
		printf("===== Enter a value greater than zero for radius. ======\n");
	}

	printf("========== Working to set up sizes ==========\n");

	imageSize = numRows * numCols * 3;
	outImage  = (unsigned char *) malloc(imageSize); //Get enough space in memory for the image

	//Open the image for writing
	if((outputFP = fopen(argv[1],"w")) == NULL ){
		printf("Error writing output file.\n");
		perror("output open error\n");
		exit(1);
	}


	printf("========== Indexing Through the given image. ==========\n");

	ptr = outImage;
	for(row = 0; row < numRows; row++){
		for(col = 0; col < numCols; col++){

			inOrOut = CheckRadius(numRows, numCols, Radius, row, col);
			inEye = Eye(numRows, numCols, row, col);
			inMouth = Mouth(numRows, numCols, Radius, row, col);
			inStem = Stem(numRows, numCols, Radius, row, col);

			if(inOrOut == 1){
				//Print Orange in the pumpkin
				*ptr	 = 215;   //R
				*(ptr+1) = 107;	//G
				*(ptr+2) = 0;	//B
			} else {
				//Print Black Pixel
				*ptr	 = 0;	//R
				*(ptr+1) = 0;	//G
				*(ptr+2) = 0;	//B
			}
			if(inEye == 0 || inMouth == 1){
				//Print Black Pixel
				*ptr	 = 0;	//R
				*(ptr+1) = 0;	//G
				*(ptr+2) = 0;	//B
			} else if(inEye == 1){
				*ptr	 = 0;	//R
				*(ptr+1) = 0;	//G
				*(ptr+2) = 255;	//B
			} else if(inEye == 2){
				//Print Red Pixel
				*ptr	 = 255;	//R
				*(ptr+1) = 0;	//G
				*(ptr+2) = 0;	//B
			} else if(inStem == 1){
				//Print stem Pixel
				*ptr	 = 100;	//R
				*(ptr+1) = 173;	//G
				*(ptr+2) = 42;	//B
			}			
			//Advance the pixel pointer
			ptr += 3;
		}
	}

	printf("========== Writing to file ==========\n");

	fprintf(outputFP, "P6 %d %d 255\n", numRows, numCols);
	fwrite(outImage, 1, imageSize, outputFP);

	//Done
	fclose(outputFP);

	return 0;
}
