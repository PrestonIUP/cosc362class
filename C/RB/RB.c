#include <stdio.h>
#include <stdlib.h>

/*
 * This is a Red and Blue plain pixel map (.ppm)  code example in C programming
 * Author: Preston Dulion	Date: 10/19/2021
 */


int main(int argc,		//The number of command line arguments.
	 char *argv[] ){	//Vector of command line arguments

	//Variable declarations

	int numRows;		//Number of rows for the image file
	int numCols;		//Number of cols for the image file
	int imageSize;		//Total size of the .ppm file image
	int row, col;		//The marker to know where we are in the image.

	unsigned char *outImage;	//Pixel pointer
	unsigned char *ptr;		//pointer
	FILE *outputFP;			//My output file pointer


	printf("========== Making a red and blue .ppm file! ========== \n");

	//Read in the command  line arguments:

	if(argc != 4){
		printf("\n===== Usage: RedBlue OutFileName numRows numCols =====\n");
		return 1; //exit(1); //can also be used
	}

	if((numRows = atoi(argv[2])) <= 0){
		printf("===== Enter a value greater than zero for rows. =====\n");
	}
	if((numCols = atoi(argv[3])) <= 0){
		printf("===== Enter a value great than zero for cols. =====\n");
	}

	printf("========== Working to set up sizes ==========\n");

	imageSize = numRows * numCols * 3;
	outImage  = (unsigned char *) malloc(imageSize); //Get enough space in memory for the image

	//Open the image for writing
	if((outputFP = fopen(argv[1],"w")) == NULL ){
		printf("Error writing output file.\n");
		perror("output open error\n");
		exit(1);
	}


	printf("========== Indexing Through the given image. ==========\n");

	ptr = outImage;
	for(row = 0; row < numRows; row++){
		for(col = 0; col < numCols; col++){
			//printf("Row and Col (%d,%d)\n",row,col);
			if(col < numCols/2){
				//Print Red Pixel
				*ptr	 = 255; //R
				*(ptr+1) = 0;	//G
				*(ptr+2) = 0;	//B
			} else {
				//Print Blue Pixel
				*ptr	 = 0;	//R
				*(ptr+1) = 0;	//G
				*(ptr+2) = 255;	//B
			}
			//Advance the pixel pointer
			ptr += 3;
		}
	}

	printf("========== Writing to file ==========\n");

	fprintf(outputFP, "P6 %d %d 255\n", numRows, numCols);
	fwrite(outImage, 1, imageSize, outputFP);

	//Done
	fclose(outputFP);

	return 0;
}
