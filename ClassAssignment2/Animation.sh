#!/bin/bash

clear

COUNTER=0
while [ $COUNTER -le 1 ]
do

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"

sleep .5
clear

echo " ____________________"
echo "|0                   |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "| 0                  |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"

sleep 1
clear


echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|  0                 |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|   0                |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|    0               |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|     0              |"
echo "|                    |"
echo "|____________________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|       0            |"
echo "|____________________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|________0___________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|________0___________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|________0___________|"

sleep 1
clear

echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|       _ _ _        |"
echo "|      /  @  \       |"
echo "|      \#^   /       |"
echo "|       \ & /        |"
echo "|        | |         |"
echo "|________| |_________|"

sleep 1
clear

echo " ____________________"
echo "|        ___         |"
echo "|      _/___\_       |"
echo "|     /   %   \      |"
echo "|     \ ^ *#  /      |"
echo "|      \ ^   /       |"
echo "|       \@# /        |"
echo "|        | |         |"
echo "|________| |_________|"

sleep 1
clear


echo " ____________________"
echo "|       _____        |"
echo "|     _/____ \_      |"
echo "|    /   %  %  \     |"
echo "|    \ ^ * #8 //     |"
echo "|     \ ^    //      |"
echo "|      \ #2 //       |"
echo "|       |   |        |"
echo "|_______|   |________|"

sleep 1
clear

RED='\033[0;31m'
NOTRED='\e[0m'
echo -e $RED
echo " ____________________"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|  CONNECTION  LOST  |"
echo "|                    |"
echo "|                    |"
echo "|                    |"
echo "|____________________|"
echo -e $NOTRED

sleep 3
clear


done
