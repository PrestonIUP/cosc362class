#!/bin/bash

HISTFILE=~/.bash_history
set -o history
echo "===========================================================================" >> log.log
date >> log.log
history >> log.log

