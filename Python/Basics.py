# This is like my fun Hello World!
# Author:	 Preston Dulion
# Date Created:	 10/5/21
# Last Modified: 10/5/21 
n = int(input("How many times should I say hi? "))

print("Start")
for i in range(n):
	# here I'm in the loop
	print("Hello World!")
# out of the loop
print("End")
