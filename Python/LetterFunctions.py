# Letter Functions will have all the needed helping hands.
# Author:	 Preston Dulion
# Date Created:	 10/5/21
# Date Modified: 10/5/21

import sys
import os
import math
from string import whitespace

# ==============================
# Define a Class:
# ==============================

class Address:
	def __init__(self,First,Last,ADLine1,ADLine2):
		self.First	 = First
		self.Last	 = Last
		self.ADLine1	 = ADLine1
		self.ADLine2	 = ADLine2

	def ShowInfo(self):
		print("Address Info")
		print("    {}{}".format(self.First, self.Last))
		print("   {}".format(self.ADLine1))
		print("   {}".format(self.ADLine2))


class GetDocumentText:
	def __init__(self, Address):

		self.Address = Address
		print("Working on doc")
		Address.ShowInfo()

		self.Head = "\\documentclass[12pt]{article} \n"
		self.Head += "\\begin{document} \n"

		self.Addr = ("\\hskip3in From the desk of \n\n"
				+ "\\h3skip3in {\\bf Preston Dulion} \n" +
				"\\vskip.5in \n \\noindent" + Address.First + Address.Last + "\\\\\ \n"
						+ Address.ADLine1 + "\\\\ \n"
				+ Address.ADLine2 + "\\\\ \n" + "\\vskip.25in \\noindent\\today \n \\vskip.25in"
				+ "Dear " + Address.First + ", \\\\\\ \n")

		self.Body = ("Thank you for adding magic to the world."
		+  "Your continued work makes childhood magical. \n\n")

		self.Sign = ("\\vskip1in \n \\hskip3 test name")
		self.Foot = "\\end{document}\n"

	def write(self):
		letterOutName = str(self.Address.Last.lstrip(' '))+".tex"
		letterFile = open(letterOutName, "w")

		letterFile.write(self.Head)
		letterFile.write(self.Addr)
		# letterFile.write("Writing to a file")
		letterFile.write(self.Body)
		letterFile.write(self.Sign)
		letterFile.write(self.Foot)

		letterFile.close()

		# print("This will write out to a file eventually.")

