
from LetterFunctions import *

""" Author:		 Preston Dulion
    Date Created:	 10/5/21
    Date Modified:	 10/5/21


    This is a script to do a mail merge ensuring we 
    write to all the needed magical beings.

    To Run: python3 MainMailMerge.py <LetterAddressList.csv>


"""


# print("MAIN")

# =======================================
# Read in the Letter Address List
# =======================================

if len(sys.argv)!= 2:
	print("To Run: python3 MainMailMerge.py LetterAddressList.csv")
else:
	print("Using addresses from", str(sys.argv[1]))
	dataFile = str(sys.argv[1])

# =======================================
# Open FILES
# =======================================

addressData = open(dataFile,"r")

# =======================================
# Read Data from the files line by line
# =======================================

print("Reading data from", dataFile)

addressList = []

for line in addressData.readlines():
	First, Last, Address1, Address2 = map(str,line.split(','))
	currentAddress = Address(First, Last, Address1, Address2)
	# currentAddress.ShowInfo()
	addressList.append(currentAddress)

# =======================================
# Write out a letter
# =======================================

for i in range(len(addressList)):
	# Write a form letter.
	Letter = GetDocumentText(addressList[i])
	Letter.write()

	compileLetter = "pdflatex " + addressList[i].Last + ".tex" 
	os.system(compileLetter)

