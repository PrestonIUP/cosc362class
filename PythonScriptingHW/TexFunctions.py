import re
import os

""" 
   This is my library of functions that can be called in the .tex writting main.
"""

class LastArray:
    def __init__(self, user, pt, ipadd, lastlog, duration):
        self.user       = user
        self.pt         = pt
        self.ipadd      = ipadd
        self.lastlog    = lastlog
        self.duration   = duration

class InputToTex:
    def __init__(self,input,file):
        self.input = input
        self.file = file
        # self.row =  self.input.user + " "
        # self.row += self.input.pt + " "
        # self.row += self.input.ipadd + " "
        # self.row += self.input.lastlog + " "
        # self.row += self.input.duration + "\n\n"



        date = self.date(self.input.lastlog)
        time = self.timeToString(self.input.duration)

        if time[1] == False:
            self.row = self.input.user + " was online at " + date + time[0] + " from " + self.input.ipadd + "\n\n"
        else:
            self.row = self.input.user + " is still online at " + date + " from IP: " + self.input.ipadd + "\n\n"

    def append(self):
        File = open(self.file,"a")
        File.write(self.row)
    
    def date(self,login):
        week = "Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday"
        month = "January, February, March, April, May, June, July, August, September, October, November, December"
        week = week.split(', ')
        month = month.split(', ')
        self.login = login
        # Thu Dec  2 21:45 - 22:33
        # 0   1   2   3   4  5
        regex = r" +"
        tempValue =  re.split(regex, self.login)
        for i in range(len(week)):
            if tempValue[0] in week[i]:
                tempValue[0] = week[i]
        for i in range(len(month)):
            if tempValue[1] in month[i]:
                tempValue[1] = month[i]

        value = tempValue[3] + " " + tempValue[0] + " " + tempValue[1] + " " + tempValue[2]

        return(value)
    def timeToString(self, time):
        self.time = time
        if "still" in time:
            return("still online", True)
        
        tempTime = self.time.strip('()')
        tempTime = tempTime.split(':')
        value = " for "
        if tempTime[0] != '00':
            value += tempTime[0] + " hours"
            if tempTime[1] != '00':
                value += " and " + tempTime[1] + " minutes"
        elif tempTime[1] != '00':
                value += tempTime[1] + " minutes"
        return(value, False)