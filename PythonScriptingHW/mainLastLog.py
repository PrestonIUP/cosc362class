from TexFunctions import *
""" This is a script to do convert the 'last' unix command into a pdf doc using LaTeX
    
    Author:         Preston Dulion
    Last Modified:  12/3/2021

    To Run: 
    [user@machine directory]$ python mainLastLog.py
"""

fileLOG     = r"lastPTD.log"
fileTEX     = r"lastLOGPTD.tex"
tempOutFile = r"tempOUT.txt"

print("Running...")
last = str(os.system("last > " + fileLOG))

open(tempOutFile, 'w') # Clears tempOutFile if it exists, if it doesnt exist, creates it.

lastLOG = open(fileLOG, 'r')
lastList =[]
for line in lastLOG.readlines():
    lastList.append(line.replace("\n", ""))

# Removes uneeded last 2 lines of the last command
lastList.pop()
lastList.pop()

texW = open("lastLOGPTD.tex", "w")

header =  "\\documentclass{article}[12pt]\n"
header += "\\usepackage{listings}\n"
header += "\\usepackage[margin=0.5in]{geometry}\n"
header += "\\begin{document}\n"
header += "\\author{Preston Dulion}\n\n"
header += "\\title{Last.log Python}\n"
header += "\\maketitle\n"
header += "\\newpage\n\n"

# I had it write to a tempOutFile as for some reason
# when it appended to the .tex file it cut off the start
# of the last log. 
header += "\\lstinputlisting{"+tempOutFile+"}\n\n"

texW.write(header)

footer = "\\end{document}"

regex = r"(pts/[0-9][0-9]*) | (:1)  | (144.80.\d?\d?\d.\d?\d?\d) |  + | (5.11.0-41-generi) | (system boot) |  (\(\d\d:\d\d\))"
lastArray =[]
for i in range(len(lastList)):
    lastTEMP =  re.split(regex, lastList[i])
    while None in lastTEMP:
        lastTEMP.remove(None)
    while '' in lastTEMP:
        lastTEMP.remove('')
    lastTEMP[0] = lastTEMP[0].strip()
    lastTEMP[1] = lastTEMP[1].strip()

    user        = lastTEMP[0]
    pt          = lastTEMP[1]
    ipadd       = lastTEMP[2]
    lastlog     = lastTEMP[3]
    duration    = lastTEMP[4]

    lastArray.append(LastArray(user, pt, ipadd, lastlog, duration))

texA = open(fileTEX, "a")



# Writes to the tempOutFile
for i in range (len(lastArray)):
    texFunction = InputToTex(lastArray[i], tempOutFile)
    texFunction.append()


texA.write(footer)
texW.close()
texA.close()

# Couldn't get python to run pdflatex

# texCompile = "pdflatex {}".format(fileTEX)
# print(texCompile)
# os.system(texCompile)
# os.system("pwd && ls")
# cleanup = fileLOG # + " " + fileTEX + " " + tempOutFile
# print("File cleanup: %s" % cleanup)
# os.system("rm " + cleanup)

print("Finished.\nTo get your PDF document run:\npdflatex %s" % fileTEX)